package actions.authentication;

import java.io.IOException;
import java.io.PrintWriter;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

/**
 * Servlet implementation class ResetForgottenPassword
 */
@WebServlet("/ResetForgottenPassword")
public class ResetForgottenPassword extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ResetForgottenPassword() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.sendError(500);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter writer=response.getWriter();
		String password=request.getParameter("new_password");
		System.out.println(password);
		Boolean success=true;
		JSONObject result=new JSONObject();
		try {
			ldap.SimpleLdapAuthentication.forgotPassword(request.getSession().getAttribute("forgot_username").toString(),password);
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			success=false;
			result.put("message","Your Password could not be reset");
		}
		if(success){
			result.put("message","Your Password has been reset");
			postgreSQLDatabase.authentication.Query.updateVercode(Long.parseLong(request.getSession().getAttribute("forgot_erp_id").toString()),null);
			result.put("redirect","login.jsp");
		}
			
		else{
			result.put("message","Your Password could not be reset");
		}
			result.put("success",success);
		
		writer.write(result.toString());
	}

}
