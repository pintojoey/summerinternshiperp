package actions.templates;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class DeleteTags
 */
@WebServlet("/Tags")
public class Tags extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Tags() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("reached to servlet");
		
		if(request.getParameter("action").equals("add")){
			String tagname=request.getParameter("tagname");
			String users=request.getParameter("users");
			String attribute=request.getParameter("attribute");
			postgreSQLDatabase.templates.Query.addTags(tagname, users, attribute);

		}
		
		if(request.getParameter("action").equals("delete")){
			Long tag_id=Long.parseLong(request.getParameter("tag_id"));
			postgreSQLDatabase.templates.Query.deleteTags(tag_id);
		}
		
		
	}

}
