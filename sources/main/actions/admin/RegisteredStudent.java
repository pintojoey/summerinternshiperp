package actions.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONArray;
import org.json.JSONObject;

import users.Student;
import exceptions.IncorrectFormatException;

/**
 * Servlet implementation class RegisteredStudent
 */
@WebServlet("/RegisteredStudent")
@MultipartConfig
public class RegisteredStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegisteredStudent() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter writer=response.getWriter();
		List<FileItem> formItems=null;
		try {
			DiskFileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);


			formItems = upload.parseRequest(request);
		} catch (FileUploadException e) {
			// TODO Auto-generated catch block
		}

		//DISPLAY
		if(request.getParameter("action").equals("display")){
			long registration_id = Long.parseLong(request.getParameter("registration_id").toString());

			try {
				JSONObject data=postgreSQLDatabase.registration.Query.getRegistrationStudentDataJSON(registration_id);
				writer.write(data.toString());
				return;
			} catch (SQLException | IncorrectFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		//ADD
		if(request.getParameter("action").equals("add")){			
			Student student=new Student();
			Iterator<FileItem> iter = formItems.iterator();
			while (iter.hasNext()) {
				FileItem item = (FileItem) iter.next();
				if (item.isFormField()) {
					String field;
					try {
						switch(item.getFieldName()){

						case "name":  System.out.println("name is"+item.getString()); student.setName(item.getString());break;
						case "first_name":student.setFirst_name(item.getString());break;
						case "middle_name":student.setMiddle_name(item.getString());break;
						case "last_name":student.setLast_name(item.getString());break;
						case "category":student.setCategory(item.getString());break;
						case "state":student.setState_eligibility(item.getString());break;
						case "phone_number":student.setMobile(item.getString());break;
						case "email":student.setEmail(item.getString());break;
						case "date_of_birth" : student.setDate_of_birth(new SimpleDateFormat("yyyy-MM-dd").parse(item.getString()));					break;
						case "program_allocated":student.setProgram_allocated(item.getString());break;
						case "status":student.setStatus(item.getString());break;
						case "pwd":student.setPwd(item.getString());break;
						case "gender":student.setGender(item.getString());break;
						case "nationality":student.setNationality(item.getString());break;
						case "guardian_name":student.setGuardian_name(item.getString());break;
						case "guardian_contact":student.setGuardian_contact(item.getString());break;
						case "guardian_address":student.setGuardian_address(item.getString());break;
						case "guardian_email":student.setGuardian_email(item.getString());break;
						case "father_name":student.setFather_name(item.getString());break;
						case "father_contact":student.setFather_contact(item.getString());break;
						case "mother_name":student.setMother_name(item.getString());break;
						case "mother_contact":student.setMother_contact(item.getString());break;
						case "permanent_address":student.setPermanent_address(item.getString());break;
						case "local_address":student.setLocal_address(item.getString());break;
						case "hostel_address":student.setHostel(item.getString());break;
						case "isHosteller":student.setHosteller(Boolean.parseBoolean(item.getString()));break;
						case "semester":student.setSemester(Integer.parseInt(item.getString()));break;
						case "entry_date" :student.setEntry_date(new SimpleDateFormat("yyyy-MM-dd").parse(item.getString()));break;
						}
					} catch (IncorrectFormatException | ParseException  | NumberFormatException ex) {
						//	request.setAttribute("message", "There was an error: " + ex.getMessage());
						//ex.printStackTrace();
						writer.append(ex.getMessage());
					}
				}
			}

			postgreSQLDatabase.admin.Query.addRegistrationStudent(student);

		}





		//DELETE

		if(request.getParameter("action").equals("delete")){
			long registration_id=Long.parseLong(request.getParameter("registration_id"));
			System.out.println(registration_id);
			postgreSQLDatabase.registration.Query.deleteRegisteredStudent(registration_id);
			return;
		}




		//UPDATE

		if(request.getParameter("action").equals("update")){
			boolean validate=true;
			JSONObject message=new JSONObject();
			JSONArray errors=new JSONArray();
			JSONObject json=new JSONObject();
			Student student=new Student();
			System.out.println("size is :"+formItems.size());
			Iterator<FileItem> iter = formItems.iterator();
			while (iter.hasNext()) {
				FileItem item = (FileItem) iter.next();
				if (item.isFormField()) {
					String field;
					try {
						switch(item.getFieldName()){

						case "registration_id": 
							student.setRegistration_id(Long.parseLong(item.getString()));
							break;
						case "name":   student.setName(item.getString());break;
						case "first_name":student.setFirst_name(item.getString());break;
						case "middle_name":student.setMiddle_name(item.getString());break;
						case "last_name":student.setLast_name(item.getString());break;
						case "category":student.setCategory(item.getString());break;
						case "state":student.setState_eligibility(item.getString());break;
						case "phone_number":student.setMobile(item.getString());break;
						case "email":student.setEmail(item.getString());break;
						case "date_of_birth" : student.setDate_of_birth(new SimpleDateFormat("yyyy-MM-dd").parse(item.getString()));break;
						case "program_allocated":student.setProgram_allocated(item.getString());break;
						case "status":student.setStatus(item.getString());break;
						case "pwd":student.setPwd(Boolean.parseBoolean(item.getString()));break;
						case "gender":student.setGender(item.getString());break;
						case "nationality":student.setNationality(item.getString());break;
						case "guardian_name":student.setGuardian_name(item.getString());break;
						case "guardian_contact": student.setGuardian_contact(item.getString());break;
						case "guardian_email":student.setGuardian_email(item.getString());break;
						case "guardian_address": student.setGuardian_address(item.getString());break;
						case "father_name":student.setFather_name(item.getString());break;
						case "father_contact":student.setFather_contact(item.getString());break;
						case "mother_name":student.setMother_name(item.getString());break;
						case "mother_contact":student.setMother_contact(item.getString());break;
						case "permanent_address":student.setPermanent_address(item.getString());break;
						case "local_address":student.setLocal_address(item.getString());break;
						case "hosteller":student.setHosteller(Boolean.parseBoolean(item.getString()));break;
						case "hostel_address":student.setHostel(item.getString());break;
						}

					}
					catch (IncorrectFormatException | ParseException |  NumberFormatException ex) {
						validate=false;
						errors.put(ex.getMessage());
						// writer.append( ex.getMessage());
					}

				}
			}
			if(validate==true){
				postgreSQLDatabase.registration.Query.updateRegisteredStudentData(student);
				message.put("success",validate);

			}
			json.put("success",message);
			json.put("error",errors );
			writer.write(json.toString());



		}





	}


}
