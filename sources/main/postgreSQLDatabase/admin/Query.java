/**
 * 
 */
package postgreSQLDatabase.admin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.postgresql.util.PGobject;

import exceptions.IncorrectFormatException;
import postgreSQLDatabase.feePayment.FeeBreakup;
import settings.database.PostgreSQLConnection;
import users.Student;
import users.User;

/**
 * @author Anita
 *
 */
public class Query {
	static Connection conn;
	private static PreparedStatement proc;
	public static void main(String[] args) throws SQLException, IncorrectFormatException {
		//getStudentsList();
	//	getUsersList();
		 getStudentsErpList() ;
		
	}
	public static ArrayList<Student> getStudentsList() throws SQLException, IncorrectFormatException {
		ArrayList<Student> students_list= null;

		
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection().prepareStatement("SELECT * from public.\"getStudentsList\"();");
			students_list = new ArrayList<Student>();
			ResultSet rs = proc.executeQuery();
			// System.out.println("hello");
			// System.out.println(proc);
		Student current=null;
			while(rs.next()){
				current=new Student();
				current.setStudent_id(rs.getString("student_id"));
				current.setName(rs.getString("name"));
				current.setSemester(rs.getInt("semester"));
				current.setBatch(rs.getString("batch"));
				students_list.add(current);
			}
			return students_list;
	
	}
	
	public static ArrayList<User> getUsersList() throws SQLException, IncorrectFormatException {
		ArrayList<User> users_list= null;

		
			PreparedStatement proc =  settings.database.PostgreSQLConnection.getConnection().prepareStatement("SELECT * from public.\"getUsersList\"();");
			users_list = new ArrayList<User>();
			ResultSet rs = proc.executeQuery();
			// System.out.println("hello");
		User current=null;
			while(rs.next()){
				current=new User();
				current.setUsername(rs.getString("username"));
				current.setUser_type(rs.getString("user_type"));
				current.setEmail(rs.getString("email"));
				current.setLast_seen(rs.getDate("last_seen"));
				users_list.add(current);
				
			}
			return users_list;
	
	}
	


		
	public static void addRegistrationStudent(Student current ) {
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection().prepareStatement("SELECT \"addRegistrationStudent\"(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
			proc.setString(1, current.getName()); //1 is the first ? (1 based counting)
			proc.setString(2, current.getFirst_name());
			proc.setString(3, current.getMiddle_name());
			proc.setString(4, current.getLast_name());
			proc.setString(5,current.getCategory());
			proc.setString(6,current.getState_eligibility());
			proc.setString(7,current.getMobile());
			proc.setString(8,current.getEmail());
			proc.setDate(9,utilities.StringFormatter.convert(new java.util.Date()));
			proc.setString(10,current.getProgram_allocated()); 
			proc.setString(11,current.getStatus());
			proc.setBoolean(12,current.isPwd());
			proc.setString(13,current.getGender());
			proc.setString(14,current.getNationality());
			proc.setString(15,current.getGuardian_name());
			proc.setString(16,current.getGuardian_contact());
			proc.setString(17,current.getGuardian_email());
			proc.setString(18,current.getGuardian_address());
			proc.setString(19,current.getFather_name());
			proc.setString(20,current.getFather_contact());
			proc.setString(21,current.getMother_name());
			proc.setString(22,current.getMother_contact());
			proc.setString(23,current.getPermanent_address());
			proc.setString(24,current.getLocal_address());
			proc.setBoolean(25,current.isHosteller());
			PGobject obj=new PGobject();
			obj.setType("json");
			obj.setValue("{}");
			proc.setObject(26,obj);
			proc.setInt(27,current.getSemester());
			System.out.println(proc);
			proc.executeQuery();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	
	
	
	
	
	
	
	public static ArrayList<Student> getStudentsErpList() throws SQLException{
		ArrayList<Student> studentsErp_list= null;


			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection().prepareStatement("SELECT * from public.\"getStudentsErpList\"();");
			studentsErp_list = new ArrayList<Student>();
			ResultSet rs = proc.executeQuery();
		Student current=null;
			while(rs.next()){
				try {
					current=new Student(rs,true);
				} catch (ParseException | IncorrectFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				studentsErp_list.add(current);
				//System.out.println(rs.getString("program_allocated"));
			}
			return studentsErp_list;
	
	}
	
	
	
	
	
	
	
	
	

	
	
	
}

	
	
	

