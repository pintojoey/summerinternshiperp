/**
 * 
 */
package postgreSQLDatabase.newsfeed;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author Shubhi
 *
 */
public class NewsfeedComments {
	private long comment_id,comment_author;
	private String comment_text;
	private Date comment_time;
	ArrayList<Long> comment_likes;
	private int comments_count;
	/**
	 * @return the comments_count
	 */
	public int getComments_count() {
		return comments_count;
	}
	/**
	 * @param comments_count the comments_count to set
	 */
	public void setComments_count(int comments_count) {
		this.comments_count = comments_count;
	}
	/**
	 * @return the comment_id
	 */
	public long getComment_id() {
		return comment_id;
	}
	/**
	 * @param comment_id the comment_id to set
	 */
	public void setComment_id(long comment_id) {
		this.comment_id = comment_id;
	}
	/**
	 * @return the comment_author
	 */
	public long getComment_author() {
		return comment_author;
	}
	/**
	 * @param comment_author the comment_author to set
	 */
	public void setComment_author(long comment_author) {
		this.comment_author = comment_author;
	}
	/**
	 * @return the comment_text
	 */
	public String getComment_text() {
		return comment_text;
	}
	/**
	 * @param comment_text the comment_text to set
	 */
	public void setComment_text(String comment_text) {
		this.comment_text = comment_text;
	}
	/**
	 * @return the comment_time
	 */
	public Date getComment_time() {
		return comment_time;
	}
	/**
	 * @param comment_time the comment_time to set
	 */
	public void setComment_time(Date comment_time) {
		this.comment_time = comment_time;
	}
	/**
	 * @return the comment_likes
	 */
	public ArrayList<Long> getComment_likes() {
		return comment_likes;
	}
	/**
	 * @param comment_likes the comment_likes to set
	 */
	public void setComment_likes(ArrayList<Long> comment_likes) {
		this.comment_likes = comment_likes;
	}

}
