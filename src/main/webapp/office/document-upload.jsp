<!DOCTYPE html>
<%@page import="users.Student"%>
<%@page import="postgreSQLDatabase.registration.Query"%>
<%@page import="org.json.JSONObject"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>IIIT KOTA | Dashboard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../plugins/select2/select2.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="../plugins/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="../plugins/ionicons/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
       	<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
<link rel="stylesheet"
	href="../plugins/alertify/alertifyjs/css/alertify.min.css">
<link rel="stylesheet"
	href="../plugins/alertify/alertifyjs/css/themes/default.min.css">
<script src="../plugins/alertify/alertifyjs/alertify.min.js"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    <style>
.example-modal .modal {
	position: relative;
	top: auto;
	bottom: auto;
	right: auto;
	left: auto;
	display: block;
	z-index: 1;
}

.example-modal .modal {
	background: transparent !important;
}
</style>
<script>


	var array = new Array();

	var used = new Array();

	var types = ["Combined PDF","10th Mark Sheet", "10th School Leaving certificate",
			"12th Mark Sheet", "12th School Leaving certificate", "Passport",
			"Ration Card", "Aadhar Card",  "Gap Certificate","JEE Allotment Letter","JEE Mains Admit Card",
			"JEE Mains Marks sheet", "Health certificate","Transfer Certificate","Character Certificate","Income Certificate","Caste certificate(SC/ST/OBC)","Certificate of physical Handicapped(if applicable)" ];

	function add(doc) {
		document.getElementsByClassName("form-control Input")[0].innerHTML = doc;
		document.getElementById("DOCUMENT").insertAdjacentHTML('beforeend',
				document.getElementById("doc").innerHTML);
		for (i = 0; i < types.length; i++) {
			if (doc == types[i]) {
				types.splice(i, 1);
				used.push(doc);
				break;
			}
		}
		document.getElementById("list").innerHTML = "";
		if (types.length > 0) {
			for (i = 0; i < types.length; i++) {
				if (types.length > 0) {
					document.getElementById("list").innerHTML += '<li onclick="add(\''
							+ types[i] + '\')">' + types[i] + '</li>';
				}
			}
		}

		if (types.length == 0) {
			$(document).ready(function() {
				$("#addButton").hide();
			});
		}
	}
	
   function checkRows(){
	   a = document.getElementById("DOCUMENT").getElementsByClassName("box box-danger direct-chat direct-chat-danger");
	  var files=0;
	  var visible=0;
	   for(i=0;i<a.length;i++){ 
			if(a[i].style.display!="none"){
				visible++;
				b_count = a[i].getElementsByClassName("btn btn-info btn-sm").length;
				c_count = a[i].getElementsByClassName("btn btn-danger btn-sm").length;
				for(var k=0;k<b_count;k++){
					if(a[i].getElementsByClassName("btn btn-info btn-sm")[k].value==""){
						a[i].getElementsByClassName("btn btn-info btn-sm")[k].setAttribute('class',"btn btn-danger btn-sm");
					}
					else {
						files++;
					}
					
				}
				for(var j=0;j<c_count;j++){
					if(a[i].getElementsByClassName("btn btn-danger btn-sm")[j].value==""){
					}else{
						a[i].getElementsByClassName("btn btn-danger btn-sm")[j].setAttribute('class',"btn btn-info btn-sm");
						files++;					
					}
					
				}
				
				
			}
	   
				
				
			 
			   //alert(a[i].getElementsByClassName("form-control Input")[0].innerHTML);
			
		}   
		if(visible>=1 && visible==files){return true;}
	   
	   else return false;
   }
    
	function del(deleted) {
		deleted = deleted.parentNode.parentNode.parentNode.childNodes[1].childNodes[1]
				.getElementsByClassName("form-control Input")[0];
		types.push(deleted.innerHTML);
		document.getElementById("list").innerHTML = "";
		if (types.length > 0) {
			$(document).ready(function() {
				$("#addButton").show();
			});
			for (i = 0; i < types.length; i++) {
				if (types.length > 0) {
					document.getElementById("list").innerHTML += '<li onclick="add(\''
							+ types[i] + '\')">' + types[i] + '</li>';
				}
			}
		}
	}

	function upload() {
		if(!checkRows()) {alertify.error("Upload atleast three documents!");return;}
		if(document.getElementById("DOCUMENT").getElementsByClassName("box box-danger direct-chat direct-chat-danger").length>=3){
		var csab_id=document.getElementById("csab_id").value;
		var data = new FormData();

		var document1 = [];
		var x = document
				.getElementsByClassName("box box-danger direct-chat direct-chat-danger");

		for (i = 1; i < x.length; i++) {
			if (x[i].style.display != "none") {
				var doc = {};
				doc["type"] = x[i].getElementsByClassName("box-header")[0]
						.getElementsByClassName("col-md-6")[0]
						.getElementsByClassName("form-group")[0]
						.getElementsByClassName("form-control Input")[0].innerHTML;
				doc["file"] = x[i].getElementsByClassName("box-header")[0]
						.getElementsByClassName("col-md-3")[0]
						.getElementsByClassName("box-tools")[0]
						.getElementsByClassName("btn btn-info btn-sm")[0].value
						.substring(12);
				var file = x[i].getElementsByClassName("box-header")[0]
						.getElementsByClassName("col-md-3")[0]
						.getElementsByClassName("box-tools")[0]
						.getElementsByClassName("btn btn-info btn-sm")[0].files[0];
				var file_name = x[i].getElementsByClassName("box-header")[0]
						.getElementsByClassName("col-md-3")[0]
						.getElementsByClassName("box-tools")[0]
						.getElementsByClassName("btn btn-info btn-sm")[0].files[0].name;
				data.append(doc["type"], file, file_name);
				document1.push(doc);

			}
		}
document.getElementById('upload').setAttribute('disabled',true);
		data.append("details", JSON.stringify(document1));
		data.append("csab_id",csab_id);
		var xmlhttp = new XMLHttpRequest();

		if (xmlhttp) {
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					var data=JSON.parse(xmlhttp.responseText);
					
					addIframe(data.reg_id);
                    	 
                        
                          
     				}
				if (xmlhttp.status == 404)
					alert("Could not connect to server");

			}
			xmlhttp.open("POST", "../UploadDocuments", true);
			
			xmlhttp.send(data);
		}

		//	alert(JSON.stringify(document1));
		
	  }
	else
		alertify.error("Upload atleast three documents!");
	}
</script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="modal fade" id="profile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	
</div>
	<div style="display: none;">
		<span id="doc">
			<div class="box box-danger direct-chat direct-chat-danger">
				<div class="box-header">
					<div class="col-md-6">
						<div class="form-group">
							<label class="form-control Input"></label>
						</div>
					</div>
					<div class="col-md-3">
						<div class="box-tools">
							<input type="file" value="" onchange="this.setAttribute('class','btn btn-info btn-sm')" class="btn btn-info btn-sm" size="10"/>
						</div>
					</div>
					<div class="col-md-3">
						<div class="box-tools pull-right">
							<button type="button" onclick="del(this)"
								class="btn btn-box-tool dropdown-toggle" title="remove"
								data-widget="remove">
								<i class="fa fa-times"></i>
							</button>

						</div>
					</div>
				</div>
			</div>
		</span>
	</div>

	<div class="wrapper">
		<%@ include file="header.jsp"%>
		<!-- Left side column. contains the logo and sidebar -->
		<%@ include file="main-sidebar.jsp"%>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Student <small>Home</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#" class="active"><i class="fa fa-dashboard"></i>Home</a></li>

				</ol>
			</section>

			<!-- Main content -->
			<div class="content-wapper">
				<section class="content">
					<div class="row">
						<div class="col-md-12">
							<div class="box box-primary">
								<div class="box-header with-border">
									<h3 class="box-title">Student Reporting</h3>
									<div><br>
										<strong>Temporary Entry ID :</strong>
										<%=request.getParameter("csab_id")%>
										<br>
										<%
											Student student = Query.getCsabStudentProfile(Long.parseLong(request.getParameter("csab_id")));
										if(!student.isReported()){
										%><br/>Name :<%=student.getName()%>
										<br/> Gender:<%=student.getGender()%>
										<br /> Program Allocated:<%=student.getProgram_allocated()%><br/>
										Email:	<%=student.getEmail()%><br />
										<input type="hidden" id="csab_id" value="<%=request.getParameter("csab_id")%>"/>
										<%}else out.println("Student has already been reported!");%>
									</div>
								</div>
								<div class="box-body no-padding" id="DOCUMENT"></div>
								<% 
								if(!student.isReported()){
										%>
								<div class="box-footer">
								
									<button type="button" class="btn btn-primary" id="upload"
										onClick="upload()">Upload</button>
									<button type="button" class="btn btn-warning btn-sm pull-right"
										id="addButton" data-toggle="dropdown">
										<i class="fa fa-plus"> Add New</i>
									</button>
									<ul class="dropdown-menu pull-right" role="menu">
										<span id="list"></span>
									</ul>
								</div>
								<%} %>
							</div>
						</div>
					</div>
					
				</section>
			</div>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<%@ include file="footer.jsp"%>
		<!-- Control Sidebar -->
		<%@ include file="control-sidebar.jsp"%>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 2.1.4 -->

	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
		$.widget.bridge('uibutton', $.ui.button);
	</script>
	<!-- Bootstrap 3.3.5 -->
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<script src="../plugins/select2/select2.full.min.js"></script>
	<!-- Slimscroll -->
	<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="../plugins/fastclick/fastclick.min.js"></script>
	<!-- AdminLTE App -->
	<script src="../dist/js/app.min.js"></script>

	<!-- AdminLTE for demo purposes -->
	<script src="../dist/js/demo.js"></script>
	<script type="text/javascript">
		$(function() {
			$(".select2").select2();
		});
		document.getElementById("list").innerHTML = "";
		for (i = 0; i < types.length; i++) {
			document.getElementById("list").innerHTML += '<li onclick="add(\''
					+ types[i] + '\')">' + types[i] + '</li>';
		}
		
		
	</script>
	<script>
function addIframe(reg_id){
	document.getElementById("profile").innerHTML = '<div style="margin-left:20%;margin-right:20%;margin-top:2%;"><iframe id="profile_pic" src="idCardImageUpload.jsp?reg_id='+reg_id+'" width="100%" height="600px" style="border:none;overflow:hidden;"></iframe></div>';
$("#profile").modal();
}
</script>

</body>
</html>