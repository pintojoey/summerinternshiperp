<!DOCTYPE html>
<%@page import="postgreSQLDatabase.authentication.Query"%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="settings.database.PostgreSQLConnection"%>
<%@page import="users.Student"%>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>IIIT KOTA | Dashboard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="../plugins/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="../plugins/ionicons/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
<link rel="stylesheet"
	href="../plugins/alertify/alertifyjs/css/alertify.min.css">
<link rel="stylesheet"
	href="../plugins/alertify/alertifyjs/css/themes/default.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
<script src="../plugins/alertify/alertifyjs/alertify.min.js"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  
  <![endif]-->

<script>
	function getPrivacy(html){
		document.getElementsByClassName("margin dropdown-toggle")[0].innerHTML = html+'<span class="caret"></span><span class="sr-only">Toggle Dropdown</span>';
	}
</script>
<style>
.example-modal .modal {
	position: relative;
	top: auto;
	bottom: auto;
	right: auto;
	left: auto;
	display: block;
	z-index: 1;
}

.example-modal .modal {
	background: transparent !important;
}
</style>

</head>
<body class="hold-transition skin-blue sidebar-mini">
	<script type="text/javascript">
	alertify.alert("Hi");
</script>
	<div class="wrapper">
		<%@ include file="header.jsp"%>
		<!-- Left side column. contains the logo and sidebar -->
		<%@ include file="main-sidebar.jsp"%>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					
				</h1>
				<ol class="breadcrumb">
					<li><a href="#" class="active"><i class="fa fa-dashboard"></i>Home</a></li>

				</ol>
			</section>

			<!-- Main content -->
			<%Student student= postgreSQLDatabase.registration.Query.getStudentProfile(Query.getUserId(request.getParameter("username"))); %>
			<!-- Main content -->
			<section class="content">

				<div class="row">
					<div class="col-md-3">

						<!-- Profile Image -->
						<div class="box box-widget widget-user-2">
							<!-- Add the bg color to the header using any of the bg-* classes -->
							<div class="widget-user-header bg-yellow">
								<div class="widget-user-image">
									<img class="img-circle" src="../dist/img/user7-128x128.jpg"
										alt="User Avatar">
								</div>
								<!-- /.widget-user-image -->
								<h3 class="widget-user-username"><%=student.getName() %></h3>
								<h5 class="widget-user-desc"><%=student.getStudent_id() %></h5>
							</div>
							<div class="box-footer no-padding">
								<ul class="nav nav-stacked">
									<li><a href="#">Friends<span
											class="pull-right badge bg-blue">
												<%=graph.Query.getFriends(Long.parseLong(session.getAttribute("erpId").toString())).size() %>
											</span></a>
									</li>
									<li><a href="#">Following <span
											class="pull-right badge bg-aqua">5</span></a></li>
									<li><a href="#">Followers <span
											class="pull-right badge bg-red">842</span></a></li>
									<li><a href=""><span> 
									<% Boolean friends = graph.Query.checkFriends(Long.parseLong(session.getAttribute("erpId").toString()),Query.getUserId(request.getParameter("username")));
										if(friends){%>
										<button class="btn btn-block btn-success">You are friends</button>
										<% 
										}
										else{
										%>
											<button class="btn btn-block btn-default" onclick="addFriend('<%=request.getParameter("username")%>')">Add Friend</button>
											<% 
										}	
										%>
											</span><br></a></li>		
								</ul>
							</div>
						</div>

						<!-- /.box -->

						<!-- About Me Box -->
						<div class="box box-primary">
							<div class="box-header with-border">
								<h3 class="box-title">About Me</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<strong><i class="fa fa-book margin-r-5"></i> Batch</strong>

								<p class="text-muted">
									<%=student.getEmail() %>
								</p>

								<hr>

								<strong><i class="fa fa-file-text-o margin-r-5"></i> ID</strong>

					<p class="text-muted"><%=student.getStudent_id() %></p>
								

								<hr>

								<strong><i class="fa fa-map-marker margin-r-5"></i>
									Address</strong>

								<p><%=student.getPermanent_address() %></p>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->

					<div class="col-md-8">

						<div class="box box-widget">
							<div class="box-header with-border">
								<div class="user-block">
									<img class="img-circle" src="../dist/img/user1-128x128.jpg"
										alt="User Image"> <span class="username"><a
										href="#">Jonathan Burke Jr.</a></span>

								</div>
							</div>
							<div class="box-body">
								<textarea type="text" class="form-control"
									style="border: none !important; height: 100px !important; resize: none !important;"
									id="exampleInputEmail1" placeholder="Your Status..."></textarea>
							</div>
							<div class="box-footer">
								<div class="btn-group pull-right">
									<button class="margin dropdown-toggle" data-toggle="dropdown">
										<i class="fa fa-globe"></i><b> Publicly</b> <span
											class="caret"></span><span class="sr-only">Toggle
											Dropdown</span>
									</button>

									<ul class="dropdown-menu" role="menu">
										<li><a onclick="getPrivacy(this.innerHTML)"
											style="cursor: pointer;"><i class="fa fa-globe"></i><b>
													Publicly</b></a></li>
										<li class="divider"></li>

										<li><a onclick="getPrivacy(this.innerHTML)"
											style="cursor: pointer;"><i class="fa fa-users"></i><b>
													Friends</b></a></li>

										<li class="divider"></li>

										<li><a onclick="getPrivacy(this.innerHTML)"
											style="cursor: pointer;"><i class="fa fa-expeditedssl"></i><b>
													Only Me</b></a></li>
									</ul>
									<button class="margin">Post</button>
								</div>
							</div>
						</div>

						<div class="box box-widget">
							<div class="box-header with-border">
								<div class="user-block">
									<img class="img-circle" src="../dist/img/user1-128x128.jpg"
										alt="User Image"> <span class="username"><a
										href="#">Jonathan Burke Jr.</a></span> <span class="description">Shared
										publicly - 7:30 PM Today</span>
								</div>
							</div>
							<div class="box-body">
								<!-- post text -->
								<p>Far far away, behind the word mountains, far from the
									countries Vokalia and Consonantia, there live the blind texts.
									Separated they live in Bookmarksgrove right at</p>

								<p>the coast of the Semantics, a large language ocean. A
									small river named Duden flows by their place and supplies it
									with the necessary regelialia. It is a paradisematic country,
									in which roasted parts of sentences fly into your mouth.</p>
								<button type="button" class="btn btn-default btn-xs">
									<i class="fa fa-share"></i> Share
								</button>
								<button type="button" class="btn btn-default btn-xs">
									<i class="fa fa-thumbs-o-up"></i> Like
								</button>
								<span class="pull-right text-muted">45 likes - 2 comments</span>
							</div>
							<!-- /.box-body -->
							<div class="box-footer">
								<form action="#" method="post">
									<img class="img-responsive img-circle img-sm"
										src="../dist/img/user4-128x128.jpg" alt="Alt Text">
									<!-- .img-push is used to add margin to elements next to floating images -->
									<div class="input-group margin">
										<input type="text" class="form-control col-xs-3"
											placeholder="Type a comment"> <span
											class="input-group-btn">
											<button type="button" class="btn btn-info btn-flat">Send</button>
										</span>
									</div>
								</form>
							</div>
							<div class="box-footer box-comments"
								style="overflow-y: scroll; height: 250px;">
								<div class="box-comment">
									<!-- User image -->
									<img class="img-circle img-sm"
										src="../dist/img/user4-128x128.jpg" alt="User Image">

									<div class="comment-text">
										<span class="username"> Maria Gonzales <span
											class="text-muted pull-right">8:03 PM Today</span>
										</span>
										<!-- /.username -->
										It is a long established fact that a reader will be distracted
										by the readable content of a page when looking at its layout.
									</div>
									<!-- /.comment-text -->
								</div>
								<!-- /.box-comment -->

								<div class="box-comment">
									<!-- User image -->
									<img class="img-circle img-sm"
										src="../dist/img/user4-128x128.jpg" alt="User Image">

									<div class="comment-text">
										<span class="username"> Maria Gonzales <span
											class="text-muted pull-right">8:03 PM Today</span>
										</span>
										<!-- /.username -->
										It is a long established fact that a reader will be distracted
										by the readable content of a page when looking at its layout.
									</div>
									<!-- /.comment-text -->
								</div>
								<!-- /.box-comment -->

								<div class="box-comment">
									<!-- User image -->
									<img class="img-circle img-sm"
										src="../dist/img/user4-128x128.jpg" alt="User Image">

									<div class="comment-text">
										<span class="username"> Maria Gonzales <span
											class="text-muted pull-right">8:03 PM Today</span>
										</span>
										<!-- /.username -->
										It is a long established fact that a reader will be distracted
										by the readable content of a page when looking at its layout.
									</div>
									<!-- /.comment-text -->
								</div>
								<!-- /.box-comment -->

								<div class="box-comment">
									<!-- User image -->
									<img class="img-circle img-sm"
										src="../dist/img/user4-128x128.jpg" alt="User Image">

									<div class="comment-text">
										<span class="username"> Paul Decaperio <span
											class="text-muted pull-right">8:03 PM Today</span>
										</span>
										<!-- /.username -->
										It is a long established fact that a reader will be distracted
										by the readable content of a page when looking at its layout.
									</div>
									<!-- /.comment-text -->
								</div>
								<!-- /.box-comment -->

							</div>
						</div>
					</div>
				</div>
			</section>

			<!-- /.content -->
			<%@ include file="chats.jsp"%>
			<script src="../dist/js/chats.js"></script>
		</div>

		<!-- /.content-wrapper -->

		<%@ include file="footer.jsp"%>
		<!-- Control Sidebar -->
		<%@ include file="control-sidebar.jsp"%>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 2.1.4 -->
	<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
	<!-- Bootstrap 3.3.5 -->
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<!-- Slimscroll -->
	<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="../plugins/fastclick/fastclick.min.js"></script>
	<!-- AdminLTE App -->
	<script src="../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
 	<script>
 	function addFriend(profile){
 	
 	var xmlhttp;
	try{
		xmlhttp = new XMLHttpRequest();
	} catch (e){
		// Internet Explorer Browsers
		try{
			xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e){
			//Browser doesn't support ajax	
				alert("Your browser is unsupported");
			}
		}
	}	
	
	if(xmlhttp){
	    xmlhttp.onreadystatechange=function() {
	    	
	        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				window.location.reload();
			}
	        if(xmlhttp.status == 404)
				alert("Could not connect to server");
			}
	    xmlhttp.open("POST","../AddFriendRequest?profile="+profile,true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	    xmlhttp.send();
	} 	
	
 	}
 	</script>

</body>
</html>