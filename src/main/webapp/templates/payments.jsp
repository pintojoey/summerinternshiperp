<%@page import="org.json.JSONObject"%>
<%@page import="postgreSQLDatabase.registration.Query"%>
<%@page import="postgreSQLDatabase.feePayment.Payment"%>

<%@page import="utilities.StringFormatter"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="users.Student"%>
<%@page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style type="text/css">
@page {
	size: A4;
}

table, tr, td, th {
	border: 1px solid black;
	border-collapse: collapse;
}
</style>
<title>IIITK | ERP</title>
</head>
<body>
	<header align="center">
	<h2>Student Payment status</h2>
	</header>
	<div class="box-body" style="font-size: 80%">
		<table id="example1" class="table table-bordered table-striped">
			<thead style="font-size: 120%">
				<tr>
					<th>Student ID</th>
					<th>Registration ID</th>
					<th>Name</th>
					<th>Category</th>
					<th>Payment Mode</th>
<th>Amount</th>
<th>Details</th>
<th>Comment</th>
				</tr>
			</thead>
			<tbody>
				<%
                ArrayList<Student> registration_list=postgreSQLDatabase.feePayment.Query.getStudentsListByTransaction();
                Iterator<Student> iterator=registration_list.iterator();
                while(iterator.hasNext()){
    			Student	current=Query.getRegistrationStudentData(iterator.next().getRegistration_id());
    			ArrayList<Payment> payments=postgreSQLDatabase.feePayment.Query.getFeePaymentBySemRegId(1, current.getRegistration_id());
    			Iterator<Payment> payment_iterator =payments.iterator();
    			while(payment_iterator.hasNext()){
				Payment pay=payment_iterator.next();
			
                %>
				<tr>
					<td><center><%=StringFormatter.
					getDisplayString(current.getStudent_id())%></center></td>
					<td><center><%=current.getRegistration_id()%></center></td>
					<td><center><%=current.getName() %></center></td>
					<td><center><%=current.getCategory()%></center></td>
					<td><%=pay.getPayment_method()%></td>
					<td><%=pay.getAmount()%></td>
					<td><%
													JSONObject details = pay.getDetails();
														Iterator<String> iterator2 = details.keys();
														while (iterator2.hasNext()) {

															String key = iterator2.next();
															out.print( key.toUpperCase() + ":" + details.get(key)
																	+ ", ");
														}
												%></td>
					<td><%=pay.getComment()%></td>
				</tr>
				<%
    			}
                }
				%>
			</tbody>
		</table>
	</div>
	<!-- /.box-body -->

	<script type="text/javascript">
window.print();
</script>
</body>
</html>